# import which Operating System to use
FROM 674283286888.dkr.ecr.us-east-1.amazonaws.com/core-infra/arc-linux:centos7
# install python
RUN yum -y update && yum -y install  python36-devel python3-pip python3-setuptools
# add new user 'oncall'
RUN useradd -m -s /bin/bash sqlexec
# copy required scripts to required location
COPY hello_world.py /u/thodupun/test
# use this directory 
WORKDIR /u/thodupun/test
# append permissions to user 'oncall'
RUN chown -R sqlexec:sqlexec /u/thodupun/test 
# run as 'oncall' user
USER sqlexec 
# create virtualenv 
RUN python3.6 -m venv /u/thodupun/env  
# create ENV variables
ENV LANG en_US.UTF-8
ENV KRB5_KTNAME  /etc/krb5.keytab/
ENV KRB5_CONFIG  /etc/krb5.conf
# command that has to be xecuted
CMD ["bash", "-c", "source /u/thodupun/env/bin/activate && python /u/thodupun/test/hello_world.py"]  